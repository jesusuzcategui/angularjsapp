const app = angular.module('app', ['ngRoute']);
app.config(function($routeProvider){
    $routeProvider
    .when('/', {templateUrl: 'homepage.html', controller: 'mainCtrl'})
    .when('/about', {templateUrl: 'aboutpage.html', controller: 'mainCtrl'});
});
app.controller('mainCtrl', function($scope){
    $scope.titleHome = "Inicio";
    $scope.titleAbout = "Sobre mi";
});